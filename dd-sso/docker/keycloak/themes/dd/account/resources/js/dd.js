$(document).ready(function() {
  $.ajax({
    type: "GET",
    url:  "https://api."+document.domain.split(/\.(.+)/)[1]+"/header/html/sso",
    success: function(data) {
      $('#header').html(data)
      lang = $('#keycloak_lang').html()
      $('#keycloak_lang').html('')
      $('#navbar-nextcloud').html(lang + $('#navbar-nextcloud').html())
    }
  })
  /* TODO: Remove and fix product_logo + document for existing instances */
  $('#dd-logo img').attr('src', "https://api."+document.domain.split(/\.(.+)/)[1]+"/img/dd.svg");
})
