#
#   Copyright © 2021,2022 IsardVDI S.L.
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import os
from typing import TYPE_CHECKING

from flask import Response, jsonify, redirect, render_template, request, url_for

from api.views.decorators import is_internal

if TYPE_CHECKING:
    from api.flaskapp import ApiFlaskApp


def setup_internal_views(app: "ApiFlaskApp") -> None:
    # TODO: This should be removed
    @app.route("/restart", methods=["GET"])
    @is_internal
    def api_restart() -> None:
        os.system("kill 1")


# @app.route('/user_menu/<format>', methods=['GET'])
# @app.route('/user_menu/<format>/<application>', methods=['GET'])
# def api_v2_user_menu(format,application=False):
#     if application == False:
#         if format == 'json':
#             if application == False:
#                 return json.dumps(menu.get_user_nenu()), 200, {'Content-Type': 'application/json'}
