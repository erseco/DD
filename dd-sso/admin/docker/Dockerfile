#
#   Copyright © 2021,2022 IsardVDI S.L.
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later
FROM alpine:3.12.0 as production
MAINTAINER isard <info@isardvdi.com>

# Ensure python dependencies
COPY admin/docker/requirements.pip3 /requirements.pip3

RUN apk add python3 py3-pip py3-pyldap~=3.2.0
RUN pip3 install --upgrade pip
RUN apk add --no-cache --virtual .build_deps \
    build-base \
    python3-dev \
    libffi-dev \
    gcc python3-dev linux-headers musl-dev postgresql-dev
RUN pip3 install --no-cache-dir -r requirements.pip3
RUN apk del .build_deps

RUN apk add --no-cache curl py3-yaml yarn libpq openssl py3-pillow

# Add catalan words list (issue with newer diceweare)
RUN wget -O /usr/lib/python3.8/site-packages/diceware/wordlists/wordlist_cat_ascii.txt https://raw.githubusercontent.com/1ma/diceware-cat/master/cat-wordlist-ascii.txt

# Add code and entrypoint
COPY admin/src /admin
COPY admin/docker/run.sh /run.sh

# Ensure www-data group and user (82 is default in alpine)
RUN addgroup -g 82 -S www-data; adduser -u 82 -D -S -G www-data www-data

# Fix directory permissions
# Ensure node dependencies too
RUN cd /admin/admin && \
	chown www-data:www-data "." && \
	mkdir -p "${NODE_MODULES_FOLDER:-node_modules}" && \
	chown www-data:www-data "${NODE_MODULES_FOLDER:-node_modules}" && \
	HOME=/tmp su -s /bin/sh -m www-data -c \
		"yarn install --modules-folder '${NODE_MODULES_FOLDER:-node_modules}'"

CMD [ "/run.sh" ]
