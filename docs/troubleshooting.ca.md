# Solucionant problemes

## Instal·lació de software:

Per generar certificats multidomini i del domini principal:
```
apt install rsync vim tmux certbot -y
DOMAIN=digitaldemocratic.net
certbot certonly --preferred-challenges dns --manual --email digitaldemocratic@$DOMAIN --agree-tos -d *.$DOMAIN,$DOMAIN

```

## Esborrar dades i/o configs

Si volem començar des de cero podem esborrar les dades i el repositori de codi (opcional)

Esborrar dades:
```
./dd-ctl reset-data

```

Esborrar dades, configs, codi i certificats:

```
cd /opt/DD/src
./dd-ctl reset-data
# Following commands RESET ALL DATA except for certificates
# execute them only if you know what you are doing
# This *will* result in DATA LOSS
 "./dd-ctl" down
 rm -rf /opt/DD/data/*
 rm -rf /opt/DD/db/*
 rm -rf '/opt/DD/src/avatars'
 rm -rf '/opt/DD/src/moodle'
 rm -rf '/opt/DD/src/nextcloud'
 rm -rf '/opt/DD/src/wordpress'

cd ..
rm -rf /opt/DD/src

hostname=test1
cp /opt/src/DD/dd.conf /opt/src/dd.conf.backup

git clone https://gitlab.com/DD-workspace/DD /opt/src/DD
cd /opt/src/DD
cp dd.conf.sample dd.conf
cp -r custom.sample custom
./securize_conf.sh
# Canvia els noms de domini de la configuració del dd pel hostname de la màquina
sed -i "s/DOMAIN=mydomain.com/DOMAIN=$hostname.digitaldemocratic.net/g" dd.conf
sed -i "s/LETSENCRYPT_DNS=/LETSENCRYPT_DNS=$hostname.digitaldemocratic.net/g" dd.conf
sed -i "s/LETSENCRYPT_EMAIL=/LETSENCRYPT_EMAIL=info@digitaldemocratic.net/g" dd.conf

./dd-ctl repo-update
```

### Problemas con el dns si no va la renovación automática, hacerlo por challenge del dns

```bash
docker exec -ti dd-sso-haproxy /bin/sh
```

y dentro del docker:

```bash
mkdir /certs/selfsigned
mv /certs/*.pem /certs/selfsigned/
cat /etc/letsencrypt/live/$DOMAIN/fullchain.pem /etc/letsencrypt/live/$DOMAIN/privkey.pem > /certs/chain.pem
exit
```

### Neteja de caché del keycloak

Fer les comandes **una a una**:

```bash
docker exec -ti dd-sso-keycloak /opt/jboss/keycloak/bin/jboss-cli.sh --connect --command='/subsystem=keycloak-server/theme=defaults/:write-attribute(name=cacheThemes,value=false)'

docker exec -ti dd-sso-keycloak /opt/jboss/keycloak/bin/jboss-cli.sh --connect --command='/subsystem=keycloak-server/theme=defaults/:write-attribute(name=cacheTemplates,value=false)'

docker exec -ti dd-sso-keycloak /opt/jboss/keycloak/bin/jboss-cli.sh --connect --command='/subsystem=keycloak-server/theme=defaults/:write-attribute(name=staticMaxAge,value=-1)'

docker exec -ti dd-sso-keycloak /opt/jboss/keycloak/bin/jboss-cli.sh --connect --command='reload'

docker exec -ti dd-sso-keycloak /opt/jboss/keycloak/bin/jboss-cli.sh --connect --command='/subsystem=keycloak-server/theme=defaults/:write-attribute(name=cacheThemes,value=true)'

docker exec -ti dd-sso-keycloak /opt/jboss/keycloak/bin/jboss-cli.sh --connect --command='/subsystem=keycloak-server/theme=defaults/:write-attribute(name=cacheTemplates,value=true)'

docker exec -ti dd-sso-keycloak /opt/jboss/keycloak/bin/jboss-cli.sh --connect --command='/subsystem=keycloak-server/theme=defaults/:write-attribute(name=staticMaxAge,value=2592000)'

docker exec -ti dd-sso-keycloak /opt/jboss/keycloak/bin/jboss-cli.sh --connect --command='reload'
```
